import logging
from itertools import chain as queryset_join

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Field
from django import forms
from datetime import datetime, timedelta

from invites.models import Attendee


class GetInfo(forms.ModelForm):


    def __init__(self, *args, **kwargs):
        super(GetInfo, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = ''
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-2'
        self.helper.field_class = 'col-lg-8'
        self.helper.add_input(Submit("submit", "Submit"))

    class Meta:
        model = Attendee
        fields = ['name', 'email', 'phone','is_attending','submitted_at']


