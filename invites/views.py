# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json

from django.http import HttpResponse
from django.shortcuts import render, redirect

# Create your views here.
from invites.forms import GetInfo
from invites.models import Attendee


def home(request):
    return HttpResponse("Entered Page")


def get_invite_info(request):
    hash_string = request.GET.get('hash_string',None)
    if hash_string:
        attendee = Attendee.objects.get(hash_string=hash_string)
        forms = GetInfo(instance=attendee)
        if request.method == "POST":
            forms = GetInfo(request.POST,instance=attendee)
            if forms.is_valid():
                forms.save()

                return render(request,'invites/get_info.html',{'forms':forms,'swal_value':1})
        return render(request,'invites/get_info.html',{'forms':forms,'swal_value':0})
