# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Attendee(models.Model):
    DELIGHT = 0
    TRYING = 1
    COULD_NOT = 2
    CHOICES = ((DELIGHT,'Delight'),(TRYING,'Trying'),(COULD_NOT,'Could Not'))
    name = models.CharField(max_length=255,null=True,blank=True)
    email = models.EmailField(null=True,blank=True)
    phone = models.IntegerField(null=True,blank=True)
    is_attending = models.IntegerField(choices=CHOICES,default=False)
    hash_string = models.CharField(max_length=255)
    data_entered_at = models.DateTimeField(auto_now_add=True)
    submitted_at = models.DateTimeField(null=True,blank=True)

