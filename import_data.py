import csv

from datetime import datetime
import requests
from invites.models import Attendee
f = open('data.csv')

reader = csv.reader(f)
count = 0

for row in reader:
    try:
          attendee = Attendee()
          name = str(row[2])[5:]
          print(name)
          attendee.name = str(row[2]).strip()
          attendee.email = str(row[3]).strip()
          attendee.hash_string = hash(str(datetime.now())+ str(attendee.name))
          attendee.save()
          count = count + 1
    except Exception, e:
        print(e.message)

f.close()
print count